import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountUserComponent } from './components/count-user/count-user.component';
import { ListUserComponent } from './components/list-user/list-user.component';

const routes: Routes = [
  {
    path: 'count-user',
    component: CountUserComponent
  },
  {
    path: 'list-user',
    component: ListUserComponent
  },
  {
    path: '',
    redirectTo: 'list-user',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
