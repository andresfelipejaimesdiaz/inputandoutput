import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.styl']
})
export class ListUserComponent implements OnInit {
  listUsers = [
    {
      nombre: "Felipe Diaz",
      edad: "18",
      genero: "M",
      dependencia: "PROGRAMACION"
    },
    {
      nombre: "Natalia Castaño",
      edad: "20",
      genero: "F",
      dependencia: "PSICOLOGIA"
    },
    {
      nombre: "Franklin German",
      edad: "21",
      genero: "M",
      dependencia: "PROGRAMACION"
    },
    {
      nombre: "Alex Delgado",
      edad: "20",
      genero: "M",
      dependencia: "PSICOLOGIA"
    },
    {
      nombre: "Andres Orozco",
      edad: "24",
      genero: "M",
      dependencia: "PSICOLOGIA"
    },
    {
      nombre: "Marlon Yepes",
      edad: "19",
      genero: "M",
      dependencia: "PROGRAMACION"
    },
    {
      nombre: "Isabela Herrera",
      edad: "16",
      genero: "F",
      dependencia: "PROGRAMACION"
    },
    {
      nombre: "Alejandra Ortega",
      edad: "25",
      genero: "F",
      dependencia: "PSICOLOGIA"
    },
    {
      nombre: "Michel Galvez",
      edad: "24",
      genero: "F",
      dependencia: "PROGRAMACION"
    },
  ]
  
  getSelectedType = 'T';

  constructor() { }

  ngOnInit(): void {
  }


  sendCountMale(): number {
    return this.listUsers.filter(item => item.genero === 'M').length;
  }
  sendCountFemale(): number {
    return this.listUsers.filter(item => item.genero === 'F').length;
  }
  sendCountProgramming(): number {
    return this.listUsers.filter(item => item.dependencia === 'PROGRAMACION').length;
  }
  sendCountPsicology(): number {
    return this.listUsers.filter(item => item.dependencia === 'PSICOLOGIA').length;
  }

  getDataChild(value: string): void {
    this.getSelectedType = value;
    console.warn('THE VALUE IS ', value);
  }
  /*getDataChildToDependency(value: string): void {
    this.getDependency = value;
    console.warn('THE VALUE DEPENDENCY IS ', value);
  }*/

}
