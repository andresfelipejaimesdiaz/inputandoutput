import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-count-user',
  templateUrl: './count-user.component.html',
  styleUrls: ['./count-user.component.styl']
})
export class CountUserComponent implements OnInit {
  selectedItem: string;

  @Input() countUserMale: number;
  @Input() countUserFemale: number;
  @Input() countUserProgramming: number;
  @Input() countUserPsicology: number;

  @Output() typeOptionEvent: EventEmitter<string> = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit(): void {
  }

  itemRadioSelected(): any {
    this.typeOptionEvent.emit(this.selectedItem);
    console.warn('SELECTED RADIO');
  }

}
